package anangkur.com.testmodulkotlinankoapi.activity.home.teams

import anangkur.com.testmodulkotlinankoapi.api.ApiRepository
import anangkur.com.testmodulkotlinankoapi.api.TheSportsDBApi
import anangkur.com.testmodulkotlinankoapi.model.TeamResponse
import com.google.gson.Gson
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg

class TeamPresenter(private val view: TeamView, private val apiRepository: ApiRepository, private val gson: Gson){
    fun getTeamList(league: String){
        view.showLoading()

        async(UI){
            val data = bg {
                gson.fromJson(apiRepository
                    .doRequest(TheSportsDBApi.getTeams(league)),
                    TeamResponse::class.java
                )
            }
            view.showTeamList(data.await().teams)
            view.hideLoading()
        }
    }
}