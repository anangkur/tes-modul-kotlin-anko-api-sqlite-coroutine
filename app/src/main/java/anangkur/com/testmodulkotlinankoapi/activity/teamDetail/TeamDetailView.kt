package anangkur.com.testmodulkotlinankoapi.activity.teamDetail

import anangkur.com.testmodulkotlinankoapi.model.Team

interface TeamDetailView {
    fun showLoading()
    fun hideLoading()
    fun showTeamDetail(data: List<Team>)
}