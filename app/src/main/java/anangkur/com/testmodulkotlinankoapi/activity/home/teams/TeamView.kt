package anangkur.com.testmodulkotlinankoapi.activity.home.teams

import anangkur.com.testmodulkotlinankoapi.model.Team

interface TeamView{
    fun showLoading()
    fun hideLoading()
    fun showTeamList(teamList: List<Team>)
}