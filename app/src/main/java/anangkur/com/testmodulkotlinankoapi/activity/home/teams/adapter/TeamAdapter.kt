package anangkur.com.testmodulkotlinankoapi.activity.home.teams.adapter

import anangkur.com.testmodulkotlinankoapi.R
import anangkur.com.testmodulkotlinankoapi.model.Team
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.squareup.picasso.Picasso
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk25.coroutines.onClick

class TeamAdapter(val teamList: List<Team>, val context: Context, private val listener: (Team) -> Unit): RecyclerView.Adapter<TeamAdapter.TeamViewHolder>(){
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): TeamViewHolder {
        return TeamViewHolder(
            TeamListUI().createView(
                AnkoContext.create(p0.context, p0)
            )
        )
    }

    override fun getItemCount(): Int {
        return teamList.size
    }

    override fun onBindViewHolder(p0: TeamViewHolder, p1: Int) {
        p0.onBindItem(teamList.get(p1), context, listener)
    }

    class TeamViewHolder(view: View): RecyclerView.ViewHolder(view){

        val team_badge: ImageView = view.find(R.id.team_badge)
        val team_name: TextView = view.find(R.id.team_name)

        fun onBindItem(team: Team, context: Context, listener: (Team) -> Unit){
            println("on bind: ")
            println("strTeamBadge: ${team.strTeamBadge}")
            println("strTeam: ${team.strTeam}")
            Picasso.with(context)
                .load(team.strTeamBadge)
                .into(team_badge)
            team_name.text = team.strTeam
            itemView.onClick { listener(team) }
        }
    }

    class TeamListUI : AnkoComponent<ViewGroup>{
        override fun createView(ui: AnkoContext<ViewGroup>): View {
            return with(ui){
                linearLayout{
                    lparams(width = matchParent, height = wrapContent)
                    padding = dip(16)
                    orientation = LinearLayout.HORIZONTAL

                    imageView{
                        id = R.id.team_badge
                    }.lparams(
                        width = dip(50),
                        height = dip(50)
                    )

                    textView{
                        id = R.id.team_name
                        textSize = 16f
                    }.lparams{
                        margin = dip(15)
                    }
                }
            }
        }

    }
}