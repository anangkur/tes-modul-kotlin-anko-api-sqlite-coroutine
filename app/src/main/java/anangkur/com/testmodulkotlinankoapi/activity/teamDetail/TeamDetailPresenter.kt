package anangkur.com.testmodulkotlinankoapi.activity.teamDetail

import anangkur.com.testmodulkotlinankoapi.api.ApiRepository
import anangkur.com.testmodulkotlinankoapi.api.TheSportsDBApi
import anangkur.com.testmodulkotlinankoapi.model.Team
import anangkur.com.testmodulkotlinankoapi.model.TeamResponse
import com.google.gson.Gson
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class TeamDetailPresenter(private val view: TeamDetailView,
                          private val apiRepository: ApiRepository,
                          private val gson: Gson
) {

    fun getTeamDetail(teamId: String){
        view.showLoading()

        async(UI){
            val data = bg{
                gson.fromJson(apiRepository
                    .doRequest(TheSportsDBApi.getTeamDetail(teamId)),
                    TeamResponse::class.java
                )
            }

            view.showTeamDetail(data.await().teams)
            view.hideLoading()
        }
    }
}