package anangkur.com.testmodulkotlinankoapi.model

data class LeagueResponse(
    val leagues: List<League>
)