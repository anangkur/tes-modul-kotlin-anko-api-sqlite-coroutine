package anangkur.com.testmodulkotlinankoapi.model

data class TeamResponse(
    val teams: List<Team>
)